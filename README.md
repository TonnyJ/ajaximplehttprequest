# Quickstart
This extension extends Nette\Http\Request for new methods for work with API.

## Added methods
### Get PUT
```sh
public function getPut($key = null, $default = null)
```
Returns data same way, like getPost method from Nette\Http\Request.

## Instalation
#### Download
The best way to install `AJAXimple/HTTPRequest` is using Composer:
```sh
$ composer require ajaximple/httprequest
```
#### Registering
You can enable the extension using your neon config:
```sh
services:
	http.requestFactory: AJAXimple\Http\RequestFactory
```
#### Injecting
You can simply inject request in Your Presenters/Services:
```php
public function __construct(AJAXimple\Http\Request $request)
{
    parent::__construct();
    ....
}
```
## Conclusion
This extension requires Nette3.1 and it is property of Antonín Jehlář © 2020-today
