<?php

declare(strict_types=1);

namespace AJAXimple\Http;

use Nette\Http\Request as NetteHttpRequest;
use Nette\Http\RequestFactory as NetteHttpRequestFactory;

class RequestFactory extends NetteHttpRequestFactory
{
	public function fromGlobals(): NetteHttpRequest
	{
		$request = parent::fromGlobals();

		return new Request(
			$request->getUrl(),
			$request->getPost(),
			$request->getFiles(),
			$request->getCookies(),
			$request->getHeaders(),
			$request->getMethod(),
			$request->getRemoteAddress(),
			$request->getRemoteHost(),
			function () use ($request) {
				return $request->getRawBody();
			},
		);
	}
}
